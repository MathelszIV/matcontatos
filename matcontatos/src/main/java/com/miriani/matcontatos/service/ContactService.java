package com.miriani.matcontatos.service;

import com.miriani.matcontatos.domain.Contact;
import com.miriani.matcontatos.repository.ContactRepository;
import com.miriani.matcontatos.repository.PersonRepository;
import com.miriani.matcontatos.specification.ContactSpecification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

import static java.util.Objects.*;

@Service
public class ContactService {
    private final PersonRepository personRepository;
    private final ContactRepository contactRepository;
    private final ContactSpecification contactSpecification;

    public ContactService(PersonRepository personRepository, ContactRepository contactRepository,
                          ContactSpecification contactSpecification) {
        this.personRepository = personRepository;
        this.contactRepository = contactRepository;
        this.contactSpecification = contactSpecification;
    }

    @Transactional(readOnly = true)
    public Contact findById(UUID contactId){
        return contactRepository.findById(contactId)
                .orElseThrow(() -> new RuntimeException("Contact not found!"));
    }

    @Transactional(readOnly = true)
    public List<Contact> findContactByIds(List<UUID> contactIds){
        return contactRepository.findContactByIds(contactIds);
    }

    @Transactional(readOnly = true)
    public Page<Contact> findAll(String name, String phone, String email, Long personId, Pageable pageable){
        return contactRepository.findAll(contactSpecification.filter(name, phone, email, personId), pageable);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    private Contact save(Contact contact){
        return contactRepository.save(contact);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public Contact create(Contact contact){
        var savedContact = this.save(new Contact(contact));

        if(nonNull(contact.getPersonId())){
            var personRestored = personRepository.findById(contact.getPersonId())
                    .orElseThrow(() -> new RuntimeException("Person not found!"));

            personRestored.addContact(savedContact);
        }

        return savedContact;
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public Contact update(Contact contact){
        var contactRestored = this.findById(contact.getId());

        contactRestored.mergeForUpdate(contact);

        return this.save(contactRestored);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public Contact delete(UUID contactId){
        var contactRestored = this.findById(contactId);

        contactRestored.setActiveContact(false);

        return this.update(contactRestored);
    }
}
