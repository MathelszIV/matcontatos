package com.miriani.matcontatos.service;

import com.miriani.matcontatos.domain.Person;
import com.miriani.matcontatos.repository.PersonRepository;
import com.miriani.matcontatos.specification.PersonSpecification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

@Service
public class PersonService {
    private final ContactService contactService;
    private final PersonRepository personRepository;
    private final PersonSpecification personSpecification;

    public PersonService(ContactService contactService, PersonRepository personRepository, PersonSpecification personSpecification) {
        this.contactService = contactService;
        this.personRepository = personRepository;
        this.personSpecification = personSpecification;
    }

    @Transactional(readOnly = true)
    public Person findById(Long personId) {
        return personRepository.findById(personId)
                .orElseThrow(() -> new RuntimeException("Person not found!"));
    }

    @Transactional(readOnly = true)
    public Page<Person> findAll(String name, String document, LocalDate dateOfBirth, Pageable pageable) {
        return personRepository.findAll(personSpecification.filter(name, document, dateOfBirth), pageable);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    private Person save(Person person) {
        return personRepository.save(person);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public Person create(Person person) {
        var savedPerson = this.save(new Person(person));

        person.getContacts().stream()
                .map(contactService::create)
                .forEach(savedPerson::addContact);

        return this.update(savedPerson);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public Person update(Person person) {
        var personRestored = this.findById(person.getId());

        personRestored.mergeForUpdate(person);

        return save(personRestored);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public Person delete(Long personId) {
        var personRestored = this.findById(personId);

        personRestored.setActivePerson(false);

        return this.update(personRestored);
    }
}
