package com.miriani.matcontatos.domain;

import jakarta.persistence.*;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "person")
public class Person implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    private static final String SEQ_GENERATOR = "person_id_seq_gen";
    private static final String SEQ_NAME = "person_id_seq";

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQ_GENERATOR)
    @SequenceGenerator(name = SEQ_GENERATOR, sequenceName = SEQ_NAME, allocationSize = 1)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "document")
    private String document;

    @Column(name = "ate_of_birth")
    private LocalDate dateOfBirth;

    @Column(name = "active_person")
    private Boolean activePerson;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "person_id")
    private List<Contact> contacts;


    public Person() {
    }

    public Person(Person person) {
        this.name = person.getName();
        this.document = person.getDocument();
        this.dateOfBirth = person.getDateOfBirth();
        this.contacts = new ArrayList<>();
        this.activePerson = true;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Boolean getActivePerson() {
        return activePerson;
    }

    public void setActivePerson(Boolean activePerson) {
        this.activePerson = activePerson;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contact) {
        this.contacts = contact;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(id, person.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public void addContact(Contact contact) {
        contacts.add(contact);
    }

    public Person mergeForUpdate(Person person){
        this.setName(person.getName());
        this.setDocument(person.getDocument());
        this.setDateOfBirth(person.getDateOfBirth());
        this.setActivePerson(person.getActivePerson());
        this.setContacts(person.getContacts());

        return this;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", document='" + document + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", activePerson=" + activePerson +
                '}';
    }
}
