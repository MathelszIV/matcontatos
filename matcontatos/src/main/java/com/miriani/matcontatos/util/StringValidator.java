package com.miriani.matcontatos.util;

import java.text.Normalizer;
import java.util.Optional;

import static java.util.Objects.isNull;

public class StringValidator {

    private StringValidator() {
    }

    public static String removeAcentoLower(String string) {
        return removeAcento(string).toLowerCase();
    }

    public static String removeAcento(String string) {
        return Optional.ofNullable(string)
                .map(str -> Normalizer.normalize(str.trim(), Normalizer.Form.NFD)
                        .replaceAll("[^\\p{ASCII}]", ""))
                .orElse("");
    }

    public static String verificaValorVazio(String valor){
        if(isNull(valor) || valor.isBlank()){
            return null;
        }

        return valor;
    }
}
