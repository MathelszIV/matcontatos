package com.miriani.matcontatos.repository;

import com.miriani.matcontatos.domain.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ContactRepository extends JpaRepository<Contact, UUID>, JpaSpecificationExecutor<Contact> {

    @Query(value = "select * from contact where id in(:contactIds) ", nativeQuery = true)
    List<Contact> findContactByIds(List<UUID> contactIds);
}
