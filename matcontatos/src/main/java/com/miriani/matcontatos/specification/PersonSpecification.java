package com.miriani.matcontatos.specification;

import com.miriani.matcontatos.domain.Person;
import com.miriani.matcontatos.specification.config.SpecificationDefault;
import com.miriani.matcontatos.util.StringValidator;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Optional;

@Component
public class PersonSpecification implements SpecificationDefault<Person> {

    private Specification<Person> namePerson(String namePerson){
        return (root, query, cb) -> cb.like(cb.function("unaccent", String.class, cb.lower(root.get("name"))),
                "%" + StringValidator.removeAcentoLower(namePerson) + "%");
    }

    private Specification<Person> documentPerson(String documentPerson){
        return (root, query, cb) -> cb.equal(root.get("document"), documentPerson);
    }

    private Specification<Person> dateOfBirthPerson(LocalDate dateOfBirthPerson){
        return (root, query, cb) -> cb.equal(root.get("dateOfBirth"), dateOfBirthPerson);
    }

    private Specification<Person> activePerson(){
        return (root, query, cb) -> cb.isTrue(root.get("activePerson"));
    }

    public Specification<Person> filter(String name, String document, LocalDate dateOfBirth){
        var builder = builder();

        builder.and(activePerson());

        Optional.ofNullable(name).map(this::namePerson).ifPresent(builder::and);
        Optional.ofNullable(document).map(this::documentPerson).ifPresent(builder::and);
        Optional.ofNullable(dateOfBirth).map(this::dateOfBirthPerson).ifPresent(builder::and);

        return builder.build();
    }
}
