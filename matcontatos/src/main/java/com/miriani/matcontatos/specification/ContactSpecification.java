package com.miriani.matcontatos.specification;

import com.miriani.matcontatos.domain.Contact;
import com.miriani.matcontatos.specification.config.SpecificationDefault;
import com.miriani.matcontatos.util.StringValidator;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ContactSpecification implements SpecificationDefault<Contact> {

    private Specification<Contact> nameContact(String nameContact){
        return (root, query, cb) -> cb.like(cb.function("unaccent", String.class, cb.lower(root.get("name"))),
                "%" + StringValidator.removeAcentoLower(nameContact) + "%");
    }

    private Specification<Contact> phoneContact(String phoneContact){
        return (root, query, cb) -> cb.equal(root.get("phone"), phoneContact);
    }

    private Specification<Contact> personContact(Long personId){
        return (root, query, cb) -> cb.equal(root.join("person").get("id"), personId);
    }

    private Specification<Contact> emailContact(String emailContact){
        return (root, query, cb) -> cb.equal(root.get("email"), emailContact);
    }

    private Specification<Contact> activeContact(){
        return (root, query, cb) -> cb.isTrue(root.get("activeContact"));
    }

    public Specification<Contact> filter(String name, String phone, String email, Long personId){
        var builder = builder();

        builder.and(activeContact());

        Optional.ofNullable(name).map(this::nameContact).ifPresent(builder::and);
        Optional.ofNullable(phone).map(this::phoneContact).ifPresent(builder::and);
        Optional.ofNullable(email).map(this::emailContact).ifPresent(builder::and);
        Optional.ofNullable(personId).map(this::personContact).ifPresent(builder::and);

        return builder.build();
    }
}
