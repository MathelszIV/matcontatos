package com.miriani.matcontatos.controller;

import com.miriani.matcontatos.domain.Contact;
import com.miriani.matcontatos.service.ContactService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "/contact")
public class ContactController {
    private final ContactService contactService;

    public ContactController(ContactService contactService) {
        this.contactService = contactService;
    }

    @GetMapping("/{id}")
    public Contact findContactById(@PathVariable UUID id){
        return contactService.findById(id);
    }

    @GetMapping("/contactnote")
    public Page<Contact> findAllContact(@RequestParam(name = "name", required = false) String name,
                                        @RequestParam(name = "phone", required = false) String phone,
                                        @RequestParam(name = "email", required = false) String email,
                                        @RequestParam(name = "personId", required = false) Long personId,
                                        @RequestParam(name = "pageable", required = false) Pageable pageable){
        return contactService.findAll(name, phone, email, personId, pageable);
    }

    @PostMapping
    public Contact createContact(@RequestBody Contact contact){
        return contactService.create(contact);
    }

    @PutMapping
    public Contact updateContact(@RequestBody Contact contact){
        return contactService.update(contact);
    }

    @PutMapping("/delete/{id}")
    public Contact deleteContact(@PathVariable UUID id) {
        return contactService.delete(id);
    }
}
