package com.miriani.matcontatos.controller;

import com.miriani.matcontatos.domain.Person;
import com.miriani.matcontatos.service.PersonService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@RequestMapping(value = "/person")
public class PersonController {
    private final PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("/{id}")
    public Person findContactById(@PathVariable Long id){
        return personService.findById(id);
    }

    @GetMapping("/contactnote")
    public Page<Person> findAllPerson(@RequestParam(name = "name") String name,
                                      @RequestParam(name = "document") String document,
                                      @RequestParam(name = "dateOfBirth") LocalDate dateOfBirth,
                                      @RequestParam(name = "pegeable")Pageable pageable){
        return personService.findAll(name, document, dateOfBirth, pageable);
    }

    @PostMapping
    public Person createPerson(@RequestBody Person person){
        return personService.create(person);
    }

    @PutMapping
    public Person updatePerson(@RequestBody Person person){
        return personService.update(person);
    }

    @PutMapping("/delete/{id}")
    public Person deletePerson(@PathVariable Long id){
        return personService.delete(id);
    }
}
